// app.js
const express = require('express');
const app = express();
const port = 8081;

app.get('/', (req, res) => {
  res.send('¡Hola Ernesto!');
});

const server = app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});

// Exportar el servidor para las pruebas
module.exports = server;
