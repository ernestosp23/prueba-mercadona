// app.test.js
const request = require('supertest');
const server = require('./app'); // Importar el servidor en lugar de la aplicación

test('should return "Hola Ernesto"', async () => {
  const response = await request(server).get('/');
  expect(response.text).toBe('¡Hola Ernesto!');
});

// Cerrar el servidor después de que las pruebas hayan terminado
afterAll((done) => {
  server.close(done);
});
